import angular from 'angular';
import uiRouter from 'angular-ui-router';

import { ComponentsModule } from './components/components.module';
import { CommonModule } from './common/common.module';
import { ShellComponent } from './shell/Shell.component';

angular
  .module('spotifyzier', [uiRouter, CommonModule, ComponentsModule])
  .component('shell', ShellComponent)
  .config(($urlRouterProvider) => {
    'ngInject';

    $urlRouterProvider.otherwise('/');

  });

import template from './Shell.tpl.html';

export const ShellComponent = {
  template,
  controller: class ShellComponent {
    constructor($state) {
      'ngInject';
      this.$state = $state;
    }
    onSearch(query) {
      this.$state.go('albums', {query: query});
    }
  }
};
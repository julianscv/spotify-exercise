import chai from 'chai';

describe('AlbumListItem test cases', function() {
  let componentController, ctrl;
  let bindings = {model: { images: ['',''] }};
  let location, rootScope;
  
  beforeEach(angular.mock.module('spotifyzier'));

  beforeEach(angular.mock.inject(function($componentController, $location, $rootScope) {
    componentController = $componentController;
    location = $location;
    rootScope = $rootScope;
  }));

  beforeEach(angular.mock.inject(function($componentController) {
    componentController = $componentController;
    ctrl = componentController('albumListItem', null, bindings);
  }));


  it('should redirect to album state when hitting view comment button', function() {
    ctrl.viewComments({ preventDefault: () => null });
    rootScope.$apply();
    chai.expect(location.path()).to.equal('/album');
  });

});
  
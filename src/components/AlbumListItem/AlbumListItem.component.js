import template from './AlbumListItem.tpl.html';

export const AlbumListItemComponent = {
  template,
  bindings: {
    model: '<',
    showBack: '<'
  },
  controller: class AlbumComponent {
    constructor($state, AlbumService, $window) {
      'ngInject';
      this.coverImage = this.model.images[1];
      this.$state = $state;
      this.AlbumService = AlbumService;
      this.$window = $window;
    }
    viewComments(e) {
      e.preventDefault();
      this.AlbumService.setAlbum(this.model);
      this.$state.go('album');
    }
    goBack() {
      this.$window.history.back();
    }
  }
};
import chai from 'chai';

describe('Album service test cases', function() {
  let sampleAlbum = {
    id: Math.floor(Math.random() * 100)
  };
  let $AlbumService, httpBackend;
  
  beforeEach(angular.mock.module('spotifyzier'));
  
  beforeEach(angular.mock.inject(function(AlbumService, $httpBackend) {
    $AlbumService = AlbumService;
    httpBackend = $httpBackend;
    $httpBackend.whenPOST('/comments').respond(200, {id: sampleAlbum.id});
    $httpBackend.whenGET('/comments/' + sampleAlbum.id).respond(200, [{},{},{}]);
  }));

  it('Should save the album provided', function() {
    chai.expect(true).to.equal(true);
    $AlbumService.setAlbum(sampleAlbum);
    let storedAlbum = $AlbumService.getAlbum();
    chai.expect( storedAlbum.id ).to.equal( sampleAlbum.id );
  });
  
  it('Should save the comment and get the album id', function() {
    $AlbumService.saveComment({}, sampleAlbum).then((data) => {
      chai.expect(data.id).to.equal(sampleAlbum.id);
    });
    httpBackend.flush();
  });
  
  it('Should get 3 comments', function() {
    $AlbumService.getComments(sampleAlbum.id).then((data) => {
      chai.expect(data.length).to.equal(3);
    });
    httpBackend.flush();
  });
});

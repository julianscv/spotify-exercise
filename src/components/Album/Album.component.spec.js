import chai from 'chai';

describe('Album test cases', function() {
  let componentController, ctrl, httpBackend;
  let bindings = {model: { id: 1, images: ['',''] }};
  
  beforeEach(angular.mock.module('spotifyzier'));

  beforeEach(angular.mock.inject(function($componentController, $httpBackend) {
    componentController = $componentController;
    httpBackend = $httpBackend;
    $httpBackend.whenPOST('/comments').respond(200);
    $httpBackend.whenGET('/comments/1').respond(200);
  }));

  beforeEach(angular.mock.inject(function($componentController) {
    componentController = $componentController;
    ctrl = componentController('album', null, bindings);
  }));

  it('should have a model', function() {
    chai.expect( ctrl.model ).to.have.property('id');
  });

  it('should toggle the panel when calling toggleCommentPanel', function() {
    chai.expect( ctrl.newComment ).to.equal(false);
    ctrl.toggleCommentPanel();
    chai.expect( ctrl.newComment ).to.equal(true);
  });

  it('should close the comment panel when saving a comment', function() {
    ctrl.newComment = true;
    ctrl.saveComment({});
    httpBackend.flush();
    chai.expect(ctrl.newComment).to.equal(false);
  });

});

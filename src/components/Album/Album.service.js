import angular from 'angular';

export function AlbumService ($http) {
  'ngInject';
  // Note: this is a workaround to replace the API call for requesting only one album.
  
  const service = {
    setAlbum,
    getAlbum,
    saveComment,
    getComments
  };

  function setAlbum(album) {
    localStorage.setItem('album', angular.toJson(album));
  }

  function getAlbum() {
    return angular.fromJson(localStorage.getItem('album'));
  }
  
  function saveComment(comment, album) {
    let data = {
      albumId: album.id,
      email: comment.email,
      text: comment.text
    };
    return $http({
      method: 'POST',
      url: '/comments',
      data
    }).then(response => response.data);
  }
  
  function getComments(albumId) {
    return $http({
      method: 'GET',
      url: '/comments/' + albumId
    }).then(response => response.data);
  }
  
  return service;
}
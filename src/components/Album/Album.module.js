import angular from 'angular';
import { AlbumComponent } from './Album.component';
import { AlbumService } from './Album.service';
import { AlbumListItemComponent } from '../AlbumListItem/AlbumListItem.component';
//
import { CommentEntryModule } from '../CommentEntry/CommentEntry.module';

export const AlbumModule = angular
  .module('AlbumModule', [
    CommentEntryModule
  ])
  .factory('AlbumService', AlbumService)
  .component('albumListItem', AlbumListItemComponent)
  .component('album', AlbumComponent)
  .config(($stateProvider) => {
    const albumState = {
      url: '/album',
      template: '<album model="$resolve.model" />',
      resolve: {
        model: function(AlbumService) {
          'ngInject';
          return AlbumService.getAlbum();
        }
      }
    };
    $stateProvider.state('album', albumState);
  })
  .name;
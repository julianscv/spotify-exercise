import template from './Album.tpl.html';

export const AlbumComponent = {
  template,
  bindings: {
    model: '<'
  },
  controller: class AlbumComponent {
    constructor(AlbumService) {
      'ngInject';
      this.AlbumService = AlbumService;
      this.newComment = false;
      this.getComments();
    }
    toggleCommentPanel() {
      this.newComment = !this.newComment;
    }
    saveComment(comment) {
      this.AlbumService.saveComment(comment, this.model).then(() => {
        this.newComment = false;
        this.getComments();
      });
    }
    getComments() {
      this.AlbumService.getComments(this.model.id).then( comments => this.comments = comments );
    }
  }
};
import chai from 'chai';

describe('AlbumList service test cases', function() {
  let $AlbumListService, httpBackend;
  let albumId = 200;
  beforeEach(angular.mock.module('spotifyzier'));

  beforeEach(angular.mock.inject(function(AlbumListService, $httpBackend) {
    $AlbumListService = AlbumListService;
    httpBackend = $httpBackend;
    $httpBackend.whenGET('/search?q=test').respond(200, {id: albumId});
  }));

  it('Should save the comment and get the album id', function() {
    $AlbumListService.searchAlbums('test').then((data) => {
      chai.expect(data.id).to.equal(albumId);
    });
    httpBackend.flush();
  });

});

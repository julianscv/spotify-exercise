export function AlbumListService ($http) {
  'ngInject';
  
  const service = {
    searchAlbums
  };

  function searchAlbums(albumName) {
    let options = {
      url: '/search',
      method: 'GET',
      cache: true,
      params: {
        q: albumName
      }
    };
    return $http(options).then(response => response.data);
  }
  
  return service;
}
import angular from 'angular';
import { AlbumListComponent } from './AlbumList.component';
import { AlbumListService } from './AlbumList.service';

export const AlbumListModule = angular
  .module('AlbumListModule', [
  ])
  .component('albumList', AlbumListComponent)
  .factory('AlbumListService', AlbumListService)
  .config(($stateProvider) => {
    'ngInject';

    // Define an state to handle albums search.
    const albumState = {
      url: '/albums/:query',
      template: '<album-list items="$resolve.albums" />',
      resolve: {
        albums: function(AlbumListService, $stateParams) {
          'ngInject';
          return AlbumListService.searchAlbums( $stateParams.query ).then( results => results.albums.items );
        }
      }
    };

    $stateProvider.state('albums', albumState);
  })
  .name;
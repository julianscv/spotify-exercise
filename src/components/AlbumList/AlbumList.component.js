import template from './AlbumList.tpl.html';

export const AlbumListComponent = {
  template,
  bindings: {
    items: '<'
  },
  controller: class AlbumListComponent {
    constructor() {
    }
  }
};
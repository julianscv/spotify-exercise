import template from './Comment.tpl.html';

export const CommentComponent = {
  template,
  bindings: {
    model: '<',
  },
  controller: class CommentComponent {
    constructor() {
      'ngInject';
    }
  }
};
import angular from 'angular';
import { CommentComponent } from './Comment.component';

export const CommentModule = angular
  .module('CommentModule', [
  ])
  .component('comment', CommentComponent)
  .name;
import angular from 'angular';
import { AlbumModule } from './Album/Album.module';
import { AlbumListModule } from './AlbumList/AlbumList.module';
import { SearchModule } from './Search/Search.module';
import { CommentModule } from './Comment/Comment.module';

export const ComponentsModule = angular
  .module('app.components', [
    AlbumModule,
    AlbumListModule,
    SearchModule,
    CommentModule
  ])
  .name;
import template from './Search.tpl.html';

export const SearchComponent = {
  template,
  bindings: {
    onSearch: '&'
  },
  controller: class SearchComponent {
    constructor() {
    }
    onSubmit() {
      this.onSearch({
        $query: this.query
      });
    }
  }
};
import sinon from 'sinon';
import chai from 'chai';

describe('Search test cases', function() {
  let componentController, ctrl;
  let bindings = {onSearch: sinon.spy()};
  
  beforeEach(angular.mock.module('spotifyzier'));

  beforeEach(angular.mock.inject(function($componentController) {
    componentController = $componentController;
  }));

  beforeEach(angular.mock.inject(function($componentController) {
    componentController = $componentController;
    ctrl = componentController('search', null, bindings);
  }));


  it('call provided method when submitting the form', function() {
    ctrl.onSubmit();
    chai.expect(bindings.onSearch.called).to.equal(true);
  });

});
  
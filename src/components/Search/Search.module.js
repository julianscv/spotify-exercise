import angular from 'angular';
import { SearchComponent } from './Search.component';

export const SearchModule = angular
  .module('SearchModule', [
  ])
  .component('search', SearchComponent)
  .name;
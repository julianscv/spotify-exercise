import angular from 'angular';
import { CommentEntryComponent } from './CommentEntry.component';

export const CommentEntryModule = angular
  .module('CommentEntryModule', [
  ])
  .component('commentEntry', CommentEntryComponent)
  .name;
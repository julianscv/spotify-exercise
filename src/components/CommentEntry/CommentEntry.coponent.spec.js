import sinon from 'sinon';
import chai from 'chai';

describe('Comment entry test cases', function() {
  let componentController, ctrl;
  let bindings = {onSave: sinon.spy()};
  
  beforeEach(angular.mock.module('spotifyzier'));

  beforeEach(angular.mock.inject(function($componentController) {
    componentController = $componentController;
  }));

  beforeEach(angular.mock.inject(function($componentController) {
    componentController = $componentController;
    ctrl = componentController('commentEntry', null, bindings);
  }));


  it('call provided method clicking save', function() {
    ctrl.handleSubmit();
    chai.expect(bindings.onSave.called).to.equal(true);
  });

});
  
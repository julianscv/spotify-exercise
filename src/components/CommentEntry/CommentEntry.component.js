import template from './CommentEntry.tpl.html';

export const CommentEntryComponent = {
  template,
  bindings: {
    onSave: '&',
  },
  controller: class CommentEntryComponent {
    constructor() {
      'ngInject';
      this.initComment();
    }
    initComment() {
      this.comment = {
        email: null,
        comment: null
      };
    }
    handleSubmit() {
      this.onSave({
        $comment: this.comment
      });
    }
  }
};